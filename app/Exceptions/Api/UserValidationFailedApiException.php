<?php

namespace App\Exceptions\Api;

use App\Exceptions\BaseHttpException;
use Symfony\Component\HttpFoundation\Response;

class UserValidationFailedApiException extends BaseHttpException
{
    protected $message = 'Users data is missing';

    public function getStatusCode()
    {
        return Response::HTTP_UNAUTHORIZED;
    }
}