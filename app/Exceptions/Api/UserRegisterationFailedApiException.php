<?php

namespace App\Exceptions\Api;

use App\Exceptions\BaseHttpException;
use Symfony\Component\HttpFoundation\Response;

class UserRegistrationFailedApiException extends BaseHttpException
{
    protected $message = 'User with given data could not be registered';

    public function getStatusCode()
    {
        return Response::HTTP_UNAUTHORIZED;
    }
}