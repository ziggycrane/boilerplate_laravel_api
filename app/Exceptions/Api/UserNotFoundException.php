<?php

namespace App\Exceptions\Api;

use App\Exceptions\BaseHttpException;
use Symfony\Component\HttpFoundation\Response;

class UserNotFoundException extends BaseHttpException
{
    protected $message = 'Users not found';

    public function getStatusCode()
    {
        return Response::HTTP_UNAUTHORIZED;
    }
}