<?php


namespace App\Exceptions\Api;


use App\Exceptions\BaseHttpException;

class ResetPasswordException extends BaseHttpException
{
    protected $message = 'Change users password';

}