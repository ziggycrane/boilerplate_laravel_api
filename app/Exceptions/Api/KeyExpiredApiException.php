<?php


namespace App\Exceptions\Api;


use App\Exceptions\BaseHttpException;
use Symfony\Component\HttpFoundation\Response;

class KeyExpiredApiException extends BaseHttpException
{
    protected $message = 'Api key has expired';

    public function getStatusCode()
    {
        return Response::HTTP_FORBIDDEN;
    }
}