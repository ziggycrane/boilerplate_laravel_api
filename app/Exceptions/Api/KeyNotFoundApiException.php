<?php


namespace App\Exceptions\Api;

use App\Exceptions\BaseHttpException;
use Symfony\Component\HttpFoundation\Response;

class KeyNotFoundApiException extends BaseHttpException
{
    protected $message = 'Api key was not found';

    public function getStatusCode()
    {
        return Response::HTTP_FORBIDDEN;
    }
}