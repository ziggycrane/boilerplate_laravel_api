<?php

namespace App\Exceptions\Api\Subscription;

use App\Exceptions\BaseHttpException;
use Symfony\Component\HttpFoundation\Response;

class SubscriptionNotFoundException extends BaseHttpException
{
    protected $message = 'Subscription not found';

    public function getStatusCode()
    {
        return Response::HTTP_NO_CONTENT;
    }
}