<?php

namespace App\Exceptions\Api\Routine;

use App\Exceptions\BaseHttpException;
use Symfony\Component\HttpFoundation\Response;

class SubscriptionCreationFailedException extends BaseHttpException
{
    protected $message = 'Subscription could not be created';

    public function getStatusCode()
    {
        return Response::HTTP_BAD_REQUEST;
    }
}