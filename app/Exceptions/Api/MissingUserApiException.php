<?php


namespace App\Exceptions\Api;


use App\Exceptions\BaseHttpException;
use Symfony\Component\HttpFoundation\Response;

class MissingUserApiException extends BaseHttpException
{
    protected $message = 'User not found';

    public function getStatusCode()
    {
        return Response::HTTP_FORBIDDEN;
    }
}