<?php

namespace App\Exceptions\Api;

use App\Exceptions\BaseHttpException;
use Symfony\Component\HttpFoundation\Response;

class UnauthorizedUserApiException extends BaseHttpException
{
    protected $message = 'User with given credentials not found';

    public function getStatusCode()
    {
        return Response::HTTP_UNAUTHORIZED;
    }
}