<?php

namespace App\Exceptions;

use Symfony\Component\HttpKernel\Exception\HttpException;

abstract class BaseHttpException extends HttpException
{
    public function __construct($message = null)
    {
        $this->message = $message ?: $this->message;
        parent::__construct(null, $this->message, null, [], 0);
    }
}