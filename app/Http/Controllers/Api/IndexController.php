<?php namespace App\Http\Controllers\Api;

use Illuminate\Http\Response;

class IndexController extends BaseController {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{

	}

	public function index()
	{
		return ['result'=>'Welcome to API v1'];
	}

}
