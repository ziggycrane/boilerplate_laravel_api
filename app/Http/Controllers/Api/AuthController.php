<?php


namespace App\Http\Controllers\Api;

use App;
use App\Exceptions\Api\UnauthorizedUserApiException;
use App\Services\FacebookService;
use App\Services\UserService;
use App\User;
use Facebook\Exceptions\FacebookSDKException;
use Illuminate\Support\Facades\Input;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\JWTAuth;

class AuthController extends Controller
{

    public function __construct() {
    }

    public function login(UserService $userService, JWTAuth $jwt)
    {
        $email = Input::get('email');
        $password = Input::get('password');

        $validate = $userService->validateBeforeBasicLogin([
            'email' => $email,
            'password' => $password
        ]);

        if ($validate->fails()) {
            $messages = $validate->messages();
            return [
                'status' => 'error',
                'message' => $messages->first()
            ];
        }

        try {
            $token = $userService->loginBasicAndGetToken($email, $password);

            return [
                'status' => 'success',
                'data' => $userService->getApiObject($jwt->user(), $token)
            ];
        } catch (UnauthorizedUserApiException $e) {
            return [
                'status' => 'error',
                'message' => $e->getMessage()
            ];
        }
    }

    public function register(UserService $userService, JWTAuth $jwt)
    {
        $email = Input::get('email');
        $firstName = Input::get('name');
        $password = Input::get('password');
        $platform = Input::get('platform', 'android');

        $validate = $userService->validateBeforeBasicRegister([
            'first_name' => $firstName,
            'email' => $email,
            'password' => $password,
            'platform' => $platform
        ]);

        if ($validate->fails()) {
            $messages = $validate->messages();
            return [
                'status' => 'error',
                'message' => $messages->first()
            ];
        }

        try {
            $token = $userService->registerBasicAndGetToken($email, $firstName, $password, $platform);

            return [
                'status' => 'success',
                'data' => $userService->getApiObject($jwt->user(), $token)
            ];
        } catch (UnauthorizedUserApiException $e) {
            return [
                'status' => 'error',
                'message' => "Failed to authorize user"
            ];
        }
    }

    public function social(FacebookService $fb, UserService $userService, JWTAuth $jwtAuth)
    {

        $user = [];
        $type = null;
        $platform = Input::get('platform', 'android');

        if (Input::get('accessToken')) {
            try {
                $fb->validateAccessToken(Input::get('accessToken'));
                $user = $fb->getUserProfile(Input::get('accessToken'));
                $type = User::TYPE_FACEBOOK;
            } catch (FacebookSDKException $e) {
                return [
                    'status' => 'error',
                    'message' => $e->getMessage()
                ];
            }
        }

        $email = !empty($user['email']) ? $user['email'] : null;
        $firstName = !empty($user['first_name']) ? $user['first_name'] : null;
        $externalId = !empty($user['id']) ? $user['id'] : null;

        if ($externalId == null || $firstName == null) {
            return [
                "status" => "error",
                "message" => "Failed to authorize user, data missing"
            ];
        }

        try {
            $user = $userService->getUserByExternalId($externalId);

            if ($user->exists()) { // login
                $token = $userService->loginSocialAndGetToken($externalId);

                return [
                    'status' => 'success',
                    'data' => $userService->getApiObject($jwtAuth->user(), $token)
                ];
            }

            // register
            if ($email != null) {
                $userWithEmail = $userService->getUserByEmail($email);
                if ($userWithEmail->exists()) {
                    // user with this email already registered with different provider
                    return [
                        'status' => 'error',
                        'message' => "An account already exists with the same email address but different sign-in credentials."
                    ];
                }
            }

            $token = $userService->registerSocialAndGetToken($email, $firstName, $type, $externalId, $platform);

            return [
                'status' => 'success',
                'data' => $userService->getApiObject($jwtAuth->user(), $token)
            ];
        } catch (UnauthorizedUserApiException $e) {
            return [
                'status' => 'error',
                'message' => "Failed to authorize user"
            ];
        }
    }

    public function renew(JWTAuth $jwtAuth) {
        try {
            $jwtAuth->getToken();

            $token = $jwtAuth->refresh();
        } catch (JWTException $e) {
            return [
                'status' => 'error',
                'message' => $e->getMessage()
            ];
        }

        return [
            'status' => 'success',
            'token' => $token
        ];
    }

    public function resetPassword(UserService $userService) {
        $email = Input::get('email');

        $validate = $userService->validateBeforeReset([
            'email' => $email,
        ]);

        if ($validate->fails()) {
            $messages = $validate->messages();
            return [
                'status' => 'error',
                'message' => $messages->first()
            ];
        }

        $user = $userService->getUserByEmail($email);

        if (!$user->exists()) {
            return [
                'status' => 'error',
                'message' => "User with such email does not exist"
            ];
        }

        if ($user->provider != User::TYPE_BASIC) {
            return [
                'status' => 'error',
                'message' => "User registered with $user->provider not password"
            ];
        }

        $userService->sendPasswordResetMail($email);
        return [
            'status' => 'success'
        ];
    }
}