<?php


namespace App\Http\Controllers\Web;

use App;
use App\Exceptions\Api\UnauthorizedUserApiException;
use App\Services\FacebookService;
use App\Services\PasswordResetService;
use App\Services\TwitterService;
use App\Services\UserService;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class AuthController extends Controller
{

    public function __construct()
    {

    }

    public function login(UserService $userService)
    {
        $email = Input::get('email');
        $password = Input::get('password');

        $validate = $userService->validateBeforeBasicLogin([
            'email' => $email,
            'password' => $password
        ]);

        if ($validate->fails()) {
            $messages = $validate->messages();
            return [
                'status' => 'error',
                'message' => $messages->first()
            ];
        }

        try {
            $token = $userService->loginBasicAndGetToken($email, $password);

            return [
                'status' => 'success',
                'data' => $userService->getApiObject(Auth::user(), $token)
            ];
        } catch (UnauthorizedUserApiException $e) {
            return [
                'status' => 'error',
                'message' => $e->getMessage()
            ];
        }
    }

    public function register(UserService $userService)
    {
        $email = Input::get('email');
        $firstName = Input::get('name');
        $password = Input::get('password');

        $validate = $userService->validateBeforeBasicRegister([
            'first_name' => $firstName,
            'email' => $email,
            'password' => $password,
        ]);

        if ($validate->fails()) {
            $messages = $validate->messages();
            return [
                'status' => 'error',
                'message' => $messages->first()
            ];
        }

        try {
            $token = $userService->registerBasicAndGetToken($email, $firstName, $password);

            return [
                'status' => 'success',
                'data' => $userService->getApiObject(Auth::user(), $token)
            ];
        } catch (UnauthorizedUserApiException $e) {
            return [
                'status' => 'error',
                'message' => "Failed to authorize user"
            ];
        }
    }

    public function social(FacebookService $fb, TwitterService $tw, UserService $userService) {
        $user = [];
        $type = null;

        if (Input::get('accessToken') && Input::get('accessSecret')) {
            $tw->setTokenAndSecret(Input::get('accessToken'), Input::get('accessSecret'));
            $user = $tw->getProfile();
            $type = User::TYPE_TWITTER;
        } else if (Input::get('accessToken')) {
            try {
                $fb->validateAccessToken(Input::get('accessToken'));
                $user = $fb->getUserProfile(Input::get('accessToken'));
                $type = User::TYPE_FACEBOOK;
            } catch (FacebookSDKException $e) {
                return [
                    'status' => 'error',
                    'message' => $e->getMessage()
                ];
            }
        }

        Log::debug($user);

        $email = !empty($user['email']) ? $user['email'] : null;
        $firstName = !empty($user['first_name']) ? $user['first_name'] : null;
        $externalId = !empty($user['id']) ? $user['id'] : null;

        if ($externalId == null || $firstName == null) {
            return [
                "status" => "error",
                "message" => "Failed to authorize user, data missing"
            ];
        }

        try {
            $user = $userService->getUserByExternalId($externalId);

            if (!$user->exists()) {

                $userWithEmail = $userService->getUserByEmail($email);
                if ($userWithEmail->exists()) {
                    // user with this email already registered with different provider
                    return [
                        'status' => 'error',
                        'message' => "An account already exists with the same email address but different sign-in credentials."
                    ];
                }

                $token = $userService->registerSocialAndGetToken($email, $firstName, $type, $externalId);
            } else {
                $token = $userService->loginSocialAndGetToken($email, $externalId);
            }

            return [
                'status' => 'success',
                'data' => $userService->getApiObject(Auth::user(), $token)
            ];
        } catch (UnauthorizedUserApiException $e) {
            return [
                'status' => 'error',
                'message' => "Failed to authorize user"
            ];
        }

    }

    public function getResetPassword($hash, Request $request) {

        return view('auth.reset')->with(
            [
                'token' => $hash,
                'email' => $request->email
            ]
        );
    }

    /**
     * Get the password reset credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function credentials(Request $request)
    {
        return $request->only(
            'email', 'password', 'password_confirmation', 'token'
        );
    }

    public function postResetPassword(UserService $userService, Request $request, PasswordResetService $passwordResetService) {

        $validate = $passwordResetService->validate($request);
        if ($validate->fails()) {
            return redirect()->back()
                ->withInput($request->only('email'))
                ->withErrors(['email' => $validate->messages()->first()]);
        }

        $response = $passwordResetService->reset($this->credentials($request), function($user, $password) {
            /* @var User $user */
            $user->forceFill([
                'password' => bcrypt($password)
            ])->save();
        });

        // If the password was successfully reset, we will redirect the user back to
        // the application's home authenticated view. If there is an error we can
        // redirect them back to where they came from with their error message.
        return $response == PasswordResetService::PASSWORD_RESET
            ? $this->sendResetResponse($response)
            : $this->sendResetFailedResponse($request, $response);
    }

    /**
     * Get the response for a failed password reset.
     *
     * @param  \Illuminate\Http\Request
     * @param  string  $response
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function sendResetFailedResponse(Request $request, $response)
    {
        return redirect()->back()
            ->withInput($request->only('email'))
            ->withErrors(['email' => trans($response)]);
    }

    /**
     * Get the response for a successful password reset.
     *
     * @param  string  $response
     * @return \Illuminate\Http\Response
     */
    protected function sendResetResponse($response)
    {
        return redirect("/password/reset/success")
            ->with('status', trans($response));
    }
}