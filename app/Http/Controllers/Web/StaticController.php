<?php


namespace App\Http\Controllers\Web;

use App;
use App\Exceptions\Api\UnauthorizedUserApiException;
use App\Services\FacebookService;
use App\Services\PasswordResetService;
use App\Services\TwitterService;
use App\Services\UserService;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class StaticController extends Controller
{

    public function __construct()
    {

    }

    public function terms() {
        return view('static.terms');
    }

    public function privacy() {
        return view('static.privacy');
    }

}