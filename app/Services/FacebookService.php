<?php namespace App\Services;

use Facebook\Facebook;
use Facebook\FacebookRequest;
use Illuminate\Config\Repository as Config;
use Illuminate\Routing\UrlGenerator as Url;

class FacebookService {
	/**
	 * @var Config
	 */
	protected $config;

	/**
	 * @var Url
	 */
	protected $url;


	protected $instance;

	/**
	 * @param Config $config
	 * @param Url $url
	 */
	public function __construct(Config $config, Url $url) {
		$this->config = $config;
		$this->url = $url;
		$this->instance = new Facebook([
			'app_id'     => env("FACEBOOK_APP_ID"),
			'app_secret' => env("FACEBOOK_APP_SECRET"),
			'default_graph_version' => 'v2.9',
		]);
	}

	/**
	 * @param $inputToken
	 *
	 * @return mixed
	 */

	public function validateAccessToken( $inputToken ) {

		$oAuth2Client = $this->instance->getOAuth2Client();

		$tokenMetadata = $oAuth2Client->debugToken($inputToken);

		return $tokenMetadata->getIsValid();
	}

	/**
	 * @param $accessToken
	 *
	 * @return mixed
	 */
	public function getUserProfile($accessToken){
		$res = $this->instance->get('/me?fields=id,first_name,email', $accessToken);
		return $res->getDecodedBody();
	}
}
