<?php
namespace App\Services;


use App;
use App\Exceptions\Api\MissingUserApiException;
use App\Exceptions\Api\UnauthorizedUserApiException;
use App\Exceptions\Api\UserRegistrationFailedApiException;
use App\Repositories\ResetPasswordTokenRepository;
use App\Repositories\UserRepository;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\JWTAuth;

class UserService
{
    /** @var UserRepository */
    private $users;

    private $jwtAuth;

    private static $basicLoginRules = [
        'email' => 'required|email',
        'password' => 'required'
    ];

    private static $socialLoginRules = [
        'email' => 'required|email',
        'external_id' => 'required',
        'platform' => 'required|in:android,ios'
    ];

    private static $basicRegisterRules = [
        'first_name' => 'required|string',
        'email' => 'required|email|unique:users',
        'password' => 'required',
        'platform' => 'required|in:android,ios'
    ];

    private static $resetRules = [
        'email' => 'required|email'
    ];

    private static $validatorMessages = [
        'required' => ':attribute is required.',
    ];

    public function __construct(
        UserRepository $userRepository,
        JWTAuth $jwtAuth
    ){
        $this->users = $userRepository;
        $this->jwtAuth = $jwtAuth;
    }

    /**
     * User has an token, lets authorize.
     * If token is valid, login user
     * @param $token
     * @return bool
     */
    public function authorizeWithToken($token)
    {
        $this->jwtAuth->setToken($token);

        return $this->jwtAuth->authenticate();
    }

    /**
     * Login in a certain user
     * @param string $email
     * @param string $password
     * @return String
     * @throws MissingUserApiException
     * @throws UnauthorizedUserApiException
     */
    public function loginBasicAndGetToken($email, $password)
    {
        try
        {
            $user = $this->getBasicUser($email, $password);

            if (!$user->exists()) {
                throw new UnauthorizedUserApiException("User not found");
            }

            $token = $this->createToken($user);

            if ( !$token ) {
                throw new UnauthorizedUserApiException("Failed to authorize user");
            }
        }
        catch (JWTException $e)
        {
            throw new UnauthorizedUserApiException("Failed to authorize user");
        }

        $this->authorizeWithToken($token);

        return $token;
    }

    /**
     * Login in a certain user
     * @param string $external_id
     * @return String
     * @throws MissingUserApiException
     * @throws UnauthorizedUserApiException
     */
    public function loginSocialAndGetToken($external_id)
    {
        try
        {
            $user = $this->getSocialUser($external_id);

            if (!$user->exists()) {
                throw new UnauthorizedUserApiException("User not found");
            }

            $token = $this->createToken($user);

            if ( !$token ) {
                throw new UnauthorizedUserApiException;
            }
        }
        catch (JWTException $e)
        {
            throw new UnauthorizedUserApiException;
        }

        $this->authorizeWithToken($token);

        return $token;
    }

    /**
     * @param $email
     * @param $firstName
     * @param $password
     * @param $platform
     * @return string
     *
     */
    public function registerBasicAndGetToken($email, $firstName, $password, $platform)
    {
        $user = $this->createBasicUser($email, $firstName, $password, $platform);

        $token = $this->createToken($user);

        $this->authorizeWithToken($token);

        return $token;
    }

    /**
     * @param $email
     * @param $firstName
     * @param $provider
     * @param null $external_id
     * @param $platform
     * @return string
     *
     */
    public function registerSocialAndGetToken($email, $firstName, $provider, $external_id, $platform)
    {
        $user = $this->createSocialUser($email, $firstName, $provider, $external_id, $platform);

        $token = $this->createToken($user);

        $this->authorizeWithToken($token);

        return $token;
    }

    /**
     * Generate an api key for an employee
     * @param \App\User $user
     * @return string
     */
    public function createToken(User $user)
    {
        return $this->jwtAuth->fromUser($user);
    }

    /**
     * @param $email
     * @param $firstName
     * @param $password
     * @param $platform
     * @return User
     * @internal param $fullName
     *
     */
    public function createBasicUser($email, $firstName, $password, $platform)
    {
        $user = $this->users->createByEmailAndPassword( $email, $password, $firstName, $platform);

        if ($user->exists()) {
            return $user;
        }

        throw new UserRegistrationFailedApiException;
    }

    /**
     * @param $email
     * @param $firstName
     * @param $provider
     * @param null $external_id
     * @param $platform
     * @return User
     * @internal param $fullName
     *
     */
    public function createSocialUser($email, $firstName, $provider, $external_id, $platform)
    {
        $user = $this->users->createSocialUser( $email, $firstName, $provider, $external_id, $platform);

        if ($user->exists()) {
            return $user;
        }

        throw new UserRegistrationFailedApiException;
    }

    public function validateBeforeBasicRegister(array $values)
    {
        return Validator::make($values, self::$basicRegisterRules, self::$validatorMessages);
    }

    public function validateBeforeBasicLogin(array $values)
    {
        return Validator::make($values,  self::$basicLoginRules, self::$validatorMessages);
    }

    public function validateSocialLogin(array $values)
    {
        return Validator::make($values,  self::$socialLoginRules, self::$validatorMessages);
    }

    public function validateBeforeReset(array $values)
    {
        return Validator::make($values,  self::$resetRules, self::$validatorMessages);
    }

    /**
     * Get user by email and password
     * @param string $email
     * @param string $password
     * @return User
     */
    public function getBasicUser($email, $password)
    {
        return $this->users->getByEmailAndPassword($email, $password);
    }

    /**
     * Get user by email and external id
     * @param $external_id
     * @return User
     */
    public function getSocialUser($external_id)
    {
        return $this->users->getByExternalId($external_id);
    }

    /**
     * Get user by email
     * @param string $email
     * @return User
     */
    public function getUserByEmail($email)
    {
        return $this->users->getByEmail($email);
    }

    /**
     * Get user by external id
     * @param string $external_id
     * @return User
     */
    public function getUserByExternalId($external_id)
    {
        return $this->users->getByExternalId($external_id);
    }

    /**
     * Get user by id
     * @param string $id
     * @return String
     */
    public function getUserById($id) {
        return $this->users->getById($id);
    }

    public function getNameById($id) {
        $user = $this->users->getById($id);

        return ($user) ? $user->name : null;
    }

    /**
     * @param User|JWTSubject $user
     * @param $token
     * @return array
     */
    public function getApiObject(User $user, $token)
    {
        return [
            'id' => $user->id,
            'name' => (string) $user->name,
            'token' => $token
        ];
    }
}