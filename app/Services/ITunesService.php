<?php namespace App\Services;

use File;
use Google_Client;
use Google_Service_AndroidPublisher;
use Google_Service_Exception;
use Log;
use Mockery\CountValidator\Exception;

class ITunesService {

    const APP_NAME = "Tinygain";

    const SHARED_SECRET = "9e0c819890b948509a35dd14b3770134";

    const SKU_PRIME = "tinygain.subscription";

    const SKU_PRIME_LIFETIME = "tinygain.life";

    const ENDPOINT_PRODUCTION = "https://buy.itunes.apple.com/verifyReceipt";

    const ENDPOINT_SANDBOX = "https://sandbox.itunes.apple.com/verifyReceipt";

    protected $instance;

    public function __construct()
    {

    }

    public function getITunesReceiptData($receiptData) {
        $request = json_encode(array("receipt-data" => $receiptData, "password" => self::SHARED_SECRET));
        $ch = curl_init(self::ENDPOINT_PRODUCTION); // TODO change to production
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
        $result = curl_exec($ch);
        curl_close($ch);

        return json_decode($result);
    }

    public function getITunesReceiptDataSandbox($receiptData) {
        $request = json_encode(array("receipt-data" => $receiptData, "password" => self::SHARED_SECRET));
        $ch = curl_init(self::ENDPOINT_SANDBOX); // TODO change to production
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
        $result = curl_exec($ch);
        curl_close($ch);

        return json_decode($result);
    }
}