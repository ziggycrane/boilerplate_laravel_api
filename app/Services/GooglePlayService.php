<?php namespace App\Services;

use File;
use Google_Client;
use Google_Service_AndroidPublisher;
use Google_Service_Exception;
use Log;
use Mockery\CountValidator\Exception;

class GooglePlayService {

    const APP_NAME = "Tinygain";

    const SERVICE_ACCOUNT_EMAIL = "routina@routina-687e9.iam.gserviceaccount.com";

    const PACKAGE_NAME = "com.ziggycrane.tinygain";

    const SKU_PRIME = "tinygain.prime";

    const SKU_LIFE = "tinygain.life";

    protected $instance;

    protected $client;

    public function __construct()
    {
        $this->instance = new Google_Service_AndroidPublisher($this->getClient());
    }

    public function getClient() {
        if ($this->client) {
            return $this->client;
        }

        $this->client = new Google_Client();
        $this->client->setApplicationName(self::APP_NAME);
        $this->client->setAuthConfig(resource_path('auth/service-account.json'));
        $this->client->setScopes([Google_Service_AndroidPublisher::ANDROIDPUBLISHER]);

        return $this->client;
    }

    public function getKey() {
        return File::get(base_path() . "/resources/data/Routina-a83d6165f697.p12");
    }

    public function getSubscription($purchaseToken) {
        try {
            $subscription = $this->instance->purchases_subscriptions->get(self::PACKAGE_NAME, self::SKU_PRIME, $purchaseToken);
        } catch (Google_Service_Exception $e) {
            Log::debug($e->getMessage());
            $subscription = null;
        } catch (Exception $e) {
            $subscription = null;
        }



        return $subscription;
    }

    public function getPurchase($purchaseToken) {
        try {
            $purchase = $this->instance->purchases_products->get(self::PACKAGE_NAME, self::SKU_LIFE, $purchaseToken);
        } catch (Google_Service_Exception $e) {
            Log::debug($e->getMessage());
            $purchase = null;
        } catch (Exception $e) {
            $purchase = null;
        }

        return $purchase;
    }
}