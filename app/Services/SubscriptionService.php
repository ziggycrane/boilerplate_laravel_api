<?php
namespace App\Services;

use App\Exceptions\Api\Subscription\SubscriptionNotFoundException;
use App\Exceptions\Api\Routine\SubscriptionCreationFailedException;
use App\Repositories\SubscriptionRepository;
use App\Subscription;
use Carbon\Carbon;
use Validator;

class SubscriptionService
{

    /* @var SubscriptionRepository */
    private $subscriptions;

    private static $rules = [
        'purchase_token' => 'required',
        'renew_at' => 'required'
    ];

    private static $validatorMessages = [
        'required' => ':attribute is required.',
    ];

    public function __construct(
        SubscriptionRepository $subscriptionRepository
    ) {
        $this->subscriptions = $subscriptionRepository;
    }

    /**
     * @param array $values
     * @return \Illuminate\Validation\Validator
     */
    public function validate(array $values)
    {
        return Validator::make($values, self::$rules, self::$validatorMessages);
    }

    /**
     * @param $userId
     * @param $token
     * @param $orderId
     * @param $expiresAt
     * @param $isRenewing
     * @return Subscription
     */
    public function create($userId, $token, $orderId, $expiresAt, $isRenewing)
    {
        /* @var $subscription Subscription */
        $subscription = $this->subscriptions->getSubscriptionByToken($token);

        if ($subscription) {
            if ($subscription->user_id != $userId) {
                throw new SubscriptionCreationFailedException;
            }

            return $this->subscriptions->update($token, $orderId, $expiresAt, $isRenewing);
        } else {
            $subscription = $this->subscriptions->create($userId, $token, $orderId, $expiresAt, $isRenewing);
        }

        if ($subscription) {
            return $subscription;
        }

        throw new SubscriptionCreationFailedException;
    }

    /**
     * @param $token
     * @return \App\Subscription
     */
    public function getSubscriptionByToken($token)
    {
        $subscription = $this->subscriptions->getSubscriptionByToken($token);

        if ($subscription != null) {
            return $subscription;
        }

        throw new SubscriptionNotFoundException;
    }

    /**
     * @param $userId
     * @return \App\Subscription
     */
    public function getSubscriptionByUserId($userId)
    {
        $subscription = $this->subscriptions->getSubscriptionByUserId($userId);

        if ($subscription != null) {
            return $subscription;
        }

        throw new SubscriptionNotFoundException;
    }

    /**
     * @param Subscription $subscription
     * @return boolean
     */
    public function expired(Subscription $subscription) {
        return Carbon::now()->gt(new Carbon($subscription->cancel_at));
    }

    public function hasActiveSubscription($userId) {
        try {
            $subscription = $this->getSubscriptionByUserId($userId);
        } catch (SubscriptionNotFoundException $e) {
            return false;
        }

        return !$this->expired($subscription);
    }

    /**
     * @param Subscription $subscription
     * @return array
     */
    public function getApiObject(Subscription $subscription) {
        $expireAt = new Carbon($subscription->expire_at);
        $cancelAt = new Carbon($subscription->cancel_at);
        return [
            'userId' => $subscription->user_id,
            'orderId' => $subscription->order_id,
            'purchaseToken' => $subscription->purchase_token,
            'expireUnix' => $expireAt->timestamp,
            'expireHuman' => $expireAt->toDateTimeString(),
            'cancelUnix' => $cancelAt->timestamp,
            'cancelHuman' => $cancelAt->toDateTimeString(),
            'isRenewing' => $subscription->is_renewing == 1 ? true : false
        ];
    }
}