<?php namespace App\Services;

use Google_Client;

class GoogleService {

    private $client;

    public function __construct()
    {
        $this->client = new Google_Client([
            'client_id' => '621408832618-ldmsc0mo8gt53v55miq824hu7p1lv0kl.apps.googleusercontent.com'
        ]);
    }

    public function validateToken($token) {
        return  $this->client->verifyIdToken($token);
    }

    public function getProfile($payload) {
        if (!$payload) {
            return [];
        }

        return [
            'id' => $payload['sub'],
            'email' => $payload['email'],
            'first_name' => !empty($payload['name']) ? $payload['name'] : $payload['email']
        ];
    }
}
