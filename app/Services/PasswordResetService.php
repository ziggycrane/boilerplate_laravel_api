<?php namespace App\Services;

use App\Repositories\ResetPasswordTokenRepository;
use App\Repositories\UserRepository;
use App\User;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Validator;

class PasswordResetService {

    const INVALID_USER = 'passwords.user';

    const INVALID_PASSWORD = 'passwords.password';

    const INVALID_TOKEN = 'passwords.token';

    const PASSWORD_RESET = 'passwords.reset';

    /* @var ResetPasswordTokenRepository */
    private $passwordTokenRepository;

    /* @var UserRepository $users */
    private $users;

    public function __construct(ResetPasswordTokenRepository $passwordTokenRepository, UserRepository $userRepository)
    {
        $this->passwordTokenRepository = $passwordTokenRepository;
        $this->users = $userRepository;
    }

    public function validate(Request $request) {
        return Validator::make($request->all(), $this->rules(), []);
    }

    public function reset($credentials, Closure $callback) {
        $user = $this->validateReset($credentials);
        $pass = $credentials['password'];

        if (!$user instanceof User) {
            return $user;
        }

        $callback($user, $pass);

        $this->passwordTokenRepository->delete($credentials['token']);

        return static::PASSWORD_RESET;
    }

    private function validateReset(array $credentials) {
        $user = $this->getUser($credentials['email']);
        if (is_null($user)) {
            return static::INVALID_USER;
        }

        if (! $this->validateNewPassword($credentials)) {
            return static::INVALID_PASSWORD;
        }

        if (! $this->passwordTokenRepository->exists($user, $credentials['token'])) {
            return static::INVALID_TOKEN;
        }

        return $user;
    }

    /**
     * Get the user for the given credentials.
     *
     * @param  array $email
     * @return \Illuminate\Contracts\Auth\CanResetPassword
     *
     * @throws \UnexpectedValueException
     */
    public function getUser($email)
    {
        $user = $this->users->getByEmail($email);

        if (!$user || $user->provider != User::TYPE_BASIC) {
            return null;
        }

        return $user;
    }

    /**
     * Determine if the passwords match for the request.
     *
     * @param  array  $credentials
     * @return bool
     */
    public function validateNewPassword(array $credentials)
    {
        if (isset($this->passwordValidator)) {
            list($password, $confirm) = [
                $credentials['password'],
                $credentials['password_confirmation'],
            ];

            return call_user_func(
                    $this->passwordValidator, $credentials) && $password === $confirm;
        }

        return $this->validatePasswordWithDefaults($credentials);
    }

    /**
     * Determine if the passwords are valid for the request.
     *
     * @param  array  $credentials
     * @return bool
     */
    protected function validatePasswordWithDefaults(array $credentials)
    {
        list($password, $confirm) = [
            $credentials['password'],
            $credentials['password_confirmation'],
        ];

        return $password === $confirm && mb_strlen($password) >= 6;
    }

    /**
     * Get the password reset validation rules.
     *
     * @return array
     */
    protected function rules()
    {
        return [
            'token' => 'required', 'email' => 'required|email',
            'password' => 'required|confirmed|min:6',
        ];
    }

}
