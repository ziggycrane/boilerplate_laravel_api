<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Contracts\JWTSubject;

/**
 * Class User
 * @package App\User
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $platform
 * @property string $provider
 * @property string $external_id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property-read User $user
 */
class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    const TYPE_FACEBOOK = 'facebook';

    const TYPE_BASIC = 'basic';

    const PLATFORM_ANDROID = 'android';

    const PLATFORM_IOS = 'ios';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'platform', 'provider', 'external_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function checkPassword($password) {
        return Hash::check( $password, $this->password );
    }

    public function exists()
    {
        return (bool)$this->id;
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier() {
        return $this->getKey(); // Eloquent Model method
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims() {
        return [
            'aud' => 'api',
        ];
    }
}
