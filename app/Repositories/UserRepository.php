<?php


namespace App\Repositories;


use App\Exceptions\Api\ResetPasswordException;
use App\Exceptions\Api\UserNotFoundException;
use App\User;
use Hash;

class UserRepository
{
    /**
     * Get user by id
     * @param string $userId
     * @return User
     */
    public function getById($userId)
    {
        return User::find($userId);
    }

    public function getByIds(array $userIds)
    {
        if (!$userIds) {
            return [];
        }

        /** @noinspection PhpUndefinedMethodInspection */
        $rows = User::whereIn('id', $userIds)->get();

        $re = [];
        foreach ($rows as $v) {
            $item = $v;
            $re[$item->id] = $item;
        }
        return $re;
    }

    public function getByEmailAndPassword($email, $password)
    {
        $userModel = User::where('email', $email)->first();

        if ($userModel && $userModel->provider == "basic" && empty($userModel->password)) {
            throw new ResetPasswordException;
        }

        if ($userModel && $userModel->checkPassword($password)) {
            return $userModel;
        }

        return new User;
    }

    public function getByEmailAndId($email, $external_id)
    {
        $userModel = User::where([
            'email' => $email,
            'external_id' => $external_id
        ])->first();

        if ($userModel) {
            return $userModel;
        }

        return new User;
    }

    public function getByEmail($email)
    {
        $userModel = User::where('email', $email)->first();

        if ($userModel) {
            return $userModel;
        }

        return new User;
    }

    public function getByExternalId($external_id)
    {
        $userModel = User::where('external_id', $external_id)->first();

        if ($userModel) {
            return $userModel;
        }

        return new User;
    }

    public function getByEmails(array $emails)
    {
        /** @var \Eloquent $query */
        $query = User::query();
        $rows = $query->whereIn('email', $emails)->get();
        $re = [];
        foreach ($rows as $v) {
            $item = $v;
            $re[$item->email] = $item;
        }
        return $re;
    }

    public function setPassword($userId, $newPassword)
    {
        /** @var User $user */
        $user = User::find($userId);
        $user->password = $newPassword;
        $user->save();
        return true;
    }

    /**
     * Find user by email and password
     *
     * @param string $email
     * @param string $password
     *
     * @param $name
     * @return User
     */
    public function createByEmailAndPassword($email, $password, $name, $platform)
    {
        $existing = $this->getByEmailAndPassword($email, $password);

        if ($existing->exists()) {
            return $existing;
        }

        $model = User::create([
            'email' => $email,
            'password' => Hash::make($password),
            'name' => $name,
            'platform' => $platform,
            'provider' => User::TYPE_BASIC
        ]);

        return $model;
    }

    /**
     * Find user by email and external id
     *
     * @param string $email
     * @param string $name
     * @param string $provider
     * @param string $external_id
     * @param string $platform
     * @return User
     */
    public function createSocialUser($email, $name, $provider, $external_id, $platform)
    {
        $existing = $this->getByExternalId($external_id);

        if ($existing->exists()) {
            return $existing;
        }

        $model = User::create([
            'email' => $email,
            'name' => $name,
            'platform' => $platform,
            'provider' => $provider,
            'external_id' => $external_id
        ]);

        return $model;
    }
}