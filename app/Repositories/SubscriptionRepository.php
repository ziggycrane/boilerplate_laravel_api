<?php


namespace App\Repositories;


use App\Favorite;
use App\Par\FavoritePar;
use App\Subscription;
use Carbon\Carbon;

class SubscriptionRepository
{

    public function getSubscriptionByToken($token) {
        return Subscription::where('purchase_token', $token)
            ->first();
    }

    public function getSubscriptionByUserId($user_id) {
        return Subscription::where('user_id', $user_id)
            ->orderBy(Subscription::ATTR_ID, 'desc')
            ->first();
    }

    public function create($userId, $token, $orderId, Carbon $expirationDate, $isRenewing) {

        $model = Subscription::create([
            'user_id' => $userId,
            'purchase_token' => $token,
            'order_id' => $orderId,
            'expire_at' => $expirationDate->toDateTimeString(),
            'cancel_at' => $expirationDate->addDays(7)->toDateTimeString(),
            'is_renewing' => $isRenewing
        ]);

        return $model;
    }

    public function update($token, $orderId, Carbon $expirationDate, $isRenewing) {
        /* @var $model Subscription */
        $model = $this->getSubscriptionByToken($token);
        $model->order_id = $orderId;
        $model->expire_at = $expirationDate->toDateTimeString();
        $model->cancel_at = $expirationDate->addDays(7)->toDateTimeString();
        $model->is_renewing = $isRenewing;
        $model->save();

        return $model;
    }
}