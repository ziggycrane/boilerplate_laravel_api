<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/**
 * Public routes
 */
$appRoutes = function() {
    Route::any('/', 'IndexController@index');
    Route::post('/login', 'AuthController@login');
    Route::post('/register', 'AuthController@register');
    Route::get('/social', 'AuthController@social');
    Route::post('/renew', 'AuthController@renew');
    Route::post('/reset', 'AuthController@resetPassword');
};

Route::group([
    'domain' => 'api.tinygain.com',
    'prefix' => 'v1',
], $appRoutes);

Route::group([
    'domain' => '192.168.0.100',
    'prefix' => 'v1',
], $appRoutes);

/**
 * Authorized routes
 */
$protectedRoutes = function() {
    Route::get('/billing/get', 'BillingController@getSubscription');
    Route::post('/billing/validate', 'BillingController@validateSubscription');
    Route::post('/billing/validate-itunes', 'BillingController@ValidateItunesReceipt');
};

Route::group([
    'domain' => 'api.tinygain.com',
    'prefix' => 'v1',
    'middleware' => 'auth:api'
], $protectedRoutes);

Route::group([
    'domain' => '192.168.0.100',
    'prefix' => 'v1',
    'middleware' => 'auth:api'
], $protectedRoutes);